 /*
                                 This is a terminal line utility to 
                                 access .shp metadata (file header)
                                 by sending shape type (to standard
                                 out) and verbose (to standard err)
                                 extracted from the first 100 bytes 
                                 of a .shp file.  

                                 It is also possible to send a .shx
                                 file to ./demystify instead -- the
                                 outcome is just about the same.

                                 Tested on an x64 and little endian 
                                 machine.

             Acknowledgements: ESRI Shapefile Technical Description
                               1997, 1998

             Copyright:       All rights reserved (c) Kenn Lui 2024
 */
#include <arpa/inet.h>
#include <errno.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include <unistd.h>
#define MIN_SIZE 104 // 100 bytes plus a 32-bit integer
#define CHARACTERISTIC_SHP_RECORD_BEGIN 50
#define CHARACTERISTIC_SHX_RECORD_BEGIN 1
typedef
enum { TBD = 0, SHP = 1, SHX = 2 }
file_type;
static char  pn[1024] = {0};
static char *ln[] = {
  "Null Shape (0)",
  "Point (1)",
  "No type - (2) is reserved for future use",
  "PolyLine (3)",
  "No type - (4) is reserved for future use",
  "Polygon (5)",
  "No type - (6) is reserved for future use",
  "No type - (7) is reserved for future use",
  "MultiPoint (8)",
  "No type - (9) is reserved for future use",
  "No type - (10) is reserved for future use",
  "PointZ (11)",
  "No type - (12) is reserved for future use",
  "PolyLineZ (13)"
  "No type - (14) is reserved for future use",
  "PolygonZ (15)"
  "No type - (16) is reserved for future use",
  "No type - (17) is reserved for future use",
  "MultiPointZ (18)",
  "No type - (19) is reserved for future use",
  "No type - (20) is reserved for future use",
  "PointM (21)",
  "No type - (22) is reserved for future use",
  "PolyLineM (23)",
  "No type - (24) is reserved for future use",
  "PolygonM (25)",
  "No type - (26) is reserved for future use",
  "No type - (27) is reserved for future use",
  "MultiPointM (28)",
  "No type - (29) is reserved for future use",
  "No type - (30) is reserved for future use",
  "MultiPatch (31)",
  "No type - (32) is reserved for future use",
  "No type - (33) is reserved for future use"
};
typedef
union {
  unsigned char bytes [MIN_SIZE];
  struct {
    int32_t  file_code    __attribute__((packed,aligned(4)));
    int32_t  unused [5]   __attribute__((packed,aligned(4)));
    int32_t  file_length  __attribute__((packed,aligned(4)));
    int32_t  version      __attribute__((packed,aligned(4)));
    int32_t  type         __attribute__((packed,aligned(4)));    
    double   x_min        __attribute__((packed,aligned(4)));
    double   y_min        __attribute__((packed,aligned(4)));
    double   x_max        __attribute__((packed,aligned(4)));
    double   y_max        __attribute__((packed,aligned(4)));
    double   z_min        __attribute__((packed,aligned(4)));
    double   z_max        __attribute__((packed,aligned(4)));
    double   m_min        __attribute__((packed,aligned(4)));
    double   m_max        __attribute__((packed,aligned(4)));
    int32_t  record_beg   __attribute__((packed,aligned(4)));        
  };
} shapefile_header;
int main( int argc, char **argv ) {
  int fd;
  int bytes_read;
  file_type ft = 0;
  shapefile_header target;
  if( argc != 2 ) {
    (void)fprintf( stderr, "Syntax: ./demystify {path to shapefile}\n" );
    return -1;
  }
  fd = open( argv[1], O_RDONLY );
  if( fd == -1 ) {
    perror( "Failure - open()" );
    return -1;
  }
  if( ( bytes_read = read(
    fd, &target, MIN_SIZE
  ) ) == -1 ) {
    perror( "Failure - read()" );
    (void)close( fd );
    return -1;
  } else if( bytes_read < MIN_SIZE ) {
    fprintf( stderr, "Failure - unable to recognize file type.\n" );
    return -1;    
  }
  (void)close( fd );
  if( ( ft = (
      ntohl( target.record_beg ) == CHARACTERISTIC_SHP_RECORD_BEGIN
    ) ? SHX
      : ntohl( target.record_beg ) == CHARACTERISTIC_SHX_RECORD_BEGIN
      ? SHP
      : 0
  ) == 0 ) {
    fprintf( stderr, "Failure - unable to recognize file type.\n" );
    return -1;
  }; 
  (void)fprintf(
    stdout, "%s", ln[target.type]
  );
  fflush( stdout );
  (void)fprintf(
    stderr,
    "\nFile type is:  \t\t %s,\n"
    "File code is:   %13d"
    "\t(ESRI standard is '9994'),\n"    
    "File length is: %13d"
    "\t16-bit words including headers,\n"
    "Version is:     %13d"
    "\t(ESRI 1998 is '1000'),\n"        
    "Shape type is:  %13s.\n"
    "Bounding box:\n"
    "\tx_min is %19f\n"
    "\tx_max is %19f\n"
    "\ty_min is %19f\n"
    "\ty_max is %19f\n"
    "\tz_min is %19f\n"
    "\tz_max is %19f\n"
    "\tm_min is %19f\n"
    "\tm_max is %19f\n"       
    "Size of %s file header:\t%zu bytes or %zu bits\n",
    (ft==2) ? ".shx" : ".shp",
    htonl( target.file_code ),
    htonl( target.file_length ),       
    target.version,
    ln[target.type],
    target.x_min,
    target.x_max,
    target.y_min,
    target.y_max,
    target.z_min,
    target.z_max,
    target.m_min,
    target.m_max,
    (ft==1) ? "main" : "index",    
    sizeof( target ) - sizeof( target.record_beg ),
    8*( sizeof( target ) - sizeof( target.record_beg )  )
  ); 
  return 0;
}
